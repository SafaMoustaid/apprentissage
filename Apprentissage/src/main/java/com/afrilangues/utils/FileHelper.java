package com.afrilangues.utils;

import java.util.Base64;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileHelper {

	public String encode(MultipartFile file) throws Exception {
		if (file != null) {
			String fileName = StringUtils.cleanPath(file.getOriginalFilename());
			if (fileName.contains("..")) {
				throw new Exception("not a valid file");
			}
			String encodeToString = Base64.getEncoder().encodeToString(file.getBytes());
			return encodeToString;
		}
		return null;
	}

}
