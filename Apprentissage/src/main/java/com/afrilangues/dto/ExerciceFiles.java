package com.afrilangues.dto;

import org.springframework.web.multipart.MultipartFile;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ExerciceFiles {
	private MultipartFile file1;
	private MultipartFile file2;
	private MultipartFile file3;
	private MultipartFile file4;
	private MultipartFile audio1;
	private MultipartFile audio2;
	private MultipartFile audio3;
	private MultipartFile audio4;
	private MultipartFile audioEcouter;
	private MultipartFile audio1ExerciceAssocier;
	private MultipartFile audio2ExerciceAssocier;
	private MultipartFile audio3ExerciceAssocier;
	private MultipartFile audioPhraseOrdonner;
}
