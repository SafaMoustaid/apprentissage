package com.afrilangues.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.afrilangues.domain.Lecon;

public interface LeconRepository extends MongoRepository<Lecon, String> {
	Lecon findByNomLecon(String nomLecon);
}
