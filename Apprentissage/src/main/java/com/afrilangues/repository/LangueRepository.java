package com.afrilangues.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.afrilangues.domain.Langue;

public interface LangueRepository extends MongoRepository<Langue, String> {
    
    Langue findByNomLangue(String nomLangue);
    Optional<Langue> findById(String id);
}
