package com.afrilangues.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.afrilangues.domain.Theme;

public interface ThemeRepository extends MongoRepository<Theme, String> {
    
    Theme findByNomTheme(String nomTheme);


}