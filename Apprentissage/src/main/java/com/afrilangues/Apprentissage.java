package com.afrilangues;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.afrilangues.domain.Role;
import com.afrilangues.repository.RoleRepository;

@SpringBootApplication
public class Apprentissage {

	public static void main(String[] args) {
		SpringApplication.run(Apprentissage.class, args);
	}

	@Bean
	CommandLineRunner init(RoleRepository roleRepository) {

		return args -> {

			Role adminRole = roleRepository.findByRole("ADMIN");
			if (adminRole == null) {
				Role newAdminRole = new Role();
				newAdminRole.setRole("ADMIN");
				roleRepository.save(newAdminRole);
			}

			Role userRole = roleRepository.findByRole("USER");
			if (userRole == null) {
				Role newUserRole = new Role();
				newUserRole.setRole("USER");
				roleRepository.save(newUserRole);
			}

			Role ProfRole = roleRepository.findByRole("PROF");
			if (ProfRole == null) {
				Role newProfRole = new Role();
				newProfRole.setRole("PROF");
				roleRepository.save(newProfRole);
			}

		};

	}
}
