package com.afrilangues.domain;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ExerciceAssocier {
	private String mot1;
	private String mot2;
	private String mot3;
	private String reponse1;
	private String reponse2;
	private String reponse3;
	private String audio1ExerciceAssocier;
	private String audio2ExerciceAssocier;
	private String audio3ExerciceAssocier;


}
