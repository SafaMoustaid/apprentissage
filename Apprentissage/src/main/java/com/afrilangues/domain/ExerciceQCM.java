package com.afrilangues.domain;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class ExerciceQCM {
	private String question;
	private String Image1;
	private String Image2;
	private String Image3;
	private String Image4;
	private String titreImage1;
	private String titreImage2;
	private String titreImage3;
	private String titreImage4;
	private String audio1;
	private String audio2;
	private String audio3;
	private String audio4;
	private String reponseTitre;
	private String reponseImage;


	

}
