package com.afrilangues.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author safa
 */
@Document(collection = "role")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Role {
	@Id
	private String id;
	private String role;
	public Role(String role) {
		super();
		this.role = role;
	}
	
	

}