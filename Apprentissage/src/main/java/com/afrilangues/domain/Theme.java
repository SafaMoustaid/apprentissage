package com.afrilangues.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "Theme")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Theme {

	@Id
	private String id;

	private String nomTheme;

	private String image;

	@DBRef
	private Langue langue;

}
