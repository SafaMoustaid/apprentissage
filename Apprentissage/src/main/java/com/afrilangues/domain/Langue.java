package com.afrilangues.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "Langue")
@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Langue {
	@Id
	private String id;

	private String nomLangue;
	private String image;

	public Langue(String nomLangue) {
		super();
		this.nomLangue = nomLangue;
	}

	public Langue orElseThrow(Object object) {
		return null;
	}

}
