package com.afrilangues.domain;

import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author safa
 */
@Document(collection = "user")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {

	@Id
	private String id;
	@Indexed(unique = true, direction = IndexDirection.DESCENDING, dropDups = true)
	private String email;
	private String password;
	private String fullname;
	private boolean enabled;
	@DBRef
	private Set<Role> roles;
	private Role role;
	private Langue langue;
	private Date dateDebut;
	private Date dateFin;
}
