package com.afrilangues.domain;

import groovy.transform.ToString;

@ToString
public enum TypeLecon {
	Vocabulaire, Phrases, Dialogue, Grammaire, Introduction;

}
