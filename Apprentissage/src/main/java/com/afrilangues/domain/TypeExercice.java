package com.afrilangues.domain;

import groovy.transform.ToString;

@ToString
public enum TypeExercice {
Ecouter,QCM,Ordonner,Associer;

}
