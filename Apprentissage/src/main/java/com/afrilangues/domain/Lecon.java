package com.afrilangues.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import groovy.transform.ToString;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "Lecon")
@Data
@ToString
@NoArgsConstructor
public class Lecon {
	@Id
	private String id;

	private String nomLecon;
	private String image;

	@DBRef
	private Theme theme;

	public Lecon(String id, String nomLecon, String image, Theme theme) {
		super();
		this.id = id;
		this.nomLecon = nomLecon;
		this.image = image;
		this.theme = theme;
	}

}
