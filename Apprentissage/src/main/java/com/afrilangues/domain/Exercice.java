package com.afrilangues.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author safa
 *
 */
@Data
@Document(collection = "Exercice")
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Exercice {
	@Id
	private String id;

	private String titreExercice;
	private String sousTitreExercice;
	@DBRef
	private Lecon lecon;
	private TypeLecon typeLecon;
	private TypeExercice typeExercice;
	private ExerciceQCM exerciceQCM;
	private ExerciceEcouter  exerciceEcouter;
    private ExerciceAssocier exerciceAssocier;
    private ExerciceOrdonner exerciceOrdonner;
}
