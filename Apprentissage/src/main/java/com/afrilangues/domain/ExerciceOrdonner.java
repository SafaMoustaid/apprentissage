package com.afrilangues.domain;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ExerciceOrdonner {
	private String phraseOrdonner;
	private String audioPhraseOrdonner;
}
