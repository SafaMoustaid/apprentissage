package com.afrilangues.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afrilangues.domain.Lecon;
import com.afrilangues.repository.LeconRepository;

@Service

public class LeconService {

	@Autowired
	private LeconRepository leconRepository;

	public Lecon findByNomLecon(String nomLecon) {
		return leconRepository.findByNomLecon(nomLecon);
	}

	public List<Lecon> getLecon() {
		return leconRepository.findAll();

	}

	public void saveLecon(Lecon lecon) {
		leconRepository.save(lecon);
	}

	public Optional<Lecon> findById(String id) {
		return leconRepository.findById(id);
	}

	public void deleteLecon(Lecon lecon) {
		leconRepository.delete(lecon);

	}

}
