package com.afrilangues.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afrilangues.domain.Theme;
import com.afrilangues.repository.ThemeRepository;

@Service
public class ThemeService {
	@Autowired
	private ThemeRepository themeRepository;

	public Theme findByNomTheme(String nomTheme) {
		return themeRepository.findByNomTheme(nomTheme);
	}

	public List<Theme> getThemes() {
		return themeRepository.findAll();

	}

	public void saveTheme(Theme theme) {
		themeRepository.save(theme);

	}

	public Optional<Theme> findById(String id) {
		return themeRepository.findById(id);
	}

	public void deleteTheme(Theme theme) {
		themeRepository.delete(theme);

	}

}
