package com.afrilangues.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afrilangues.domain.Exercice;
import com.afrilangues.repository.ExerciceRepository;

@Service
public class ExerciceService {
	@Autowired
	private ExerciceRepository exerciceRepository;

	public Exercice findByTitreExercice(String titreExercice) {
		return ExerciceRepository.findByTitreExercice(titreExercice);

	}

	public List<Exercice> getExercice() {
		return exerciceRepository.findAll();

	}

	public void saveExercice(Exercice exercice) {
		exerciceRepository.save(exercice);
	}

	public Optional<Exercice> findById(String id) {
		return exerciceRepository.findById(id);
	}

	public void deleteExercice(Exercice exercice) {
		exerciceRepository.delete(exercice);

	}

}
