package com.afrilangues.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afrilangues.domain.Langue;
import com.afrilangues.repository.LangueRepository;

@Service
public class LangueService {
	@Autowired
	private LangueRepository langueRepository;

	public Langue findByNomLangue(String nomLangue) {
		return langueRepository.findByNomLangue(nomLangue);
	}

	public void saveLangue(Langue langue) {
		langueRepository.save(langue);

	}

	public List<Langue> getLangues() {
		return langueRepository.findAll();

	}

	public Optional<Langue> findById(String id) {
		return langueRepository.findById(id);
	}

	public void deleteLangue(Langue langue) {
		langueRepository.delete(langue);

	}

}
