package com.afrilangues.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afrilangues.domain.User;
import com.afrilangues.repository.UserRepository;
import com.afrilangues.service.CustomUserDetailsService;

@RestController
public class LoginRemote {

	@Autowired
	private CustomUserDetailsService userService;
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/signup-remote", method = RequestMethod.POST)
	public void createNewUser(@Valid @RequestBody User user, BindingResult bindingResult) {
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user",
					"There is already a user registered with the username provided");
		}
		userRepository.save(user);

	}

	@RequestMapping(value = "/getIm", method = RequestMethod.GET)
	public String tt() {
		return "Get test";

	}

}
