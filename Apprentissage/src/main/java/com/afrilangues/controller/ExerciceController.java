package com.afrilangues.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afrilangues.domain.Exercice;
import com.afrilangues.domain.Lecon;
import com.afrilangues.dto.ExerciceFiles;
import com.afrilangues.repository.LeconRepository;
import com.afrilangues.service.ExerciceService;
import com.afrilangues.utils.FileHelper;

@Controller
public class ExerciceController {

	@Autowired
	private ExerciceService exerciceService;
	@Autowired
	private LeconRepository leconRepository;

	@Autowired
	private FileHelper fileHelper;

	private static List<Exercice> exercices = new ArrayList<Exercice>();

	@RequestMapping(value = "/listExercice", method = RequestMethod.GET)
	public ModelAndView AfficheLecon() {
		ModelAndView modelAndView = new ModelAndView();
		exercices = exerciceService.getExercice();
		modelAndView.addObject("exercices", exercices);
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterExercice", method = RequestMethod.GET)
	public ModelAndView AjouterLecon() {
		List<Lecon> leconListe = leconRepository.findAll();
		ModelAndView modelAndView = new ModelAndView();
		Exercice exercice = new Exercice();
		modelAndView.addObject("exercice", exercice);
		modelAndView.addObject("lecons", leconListe);

		modelAndView.setViewName("AjouterExercice");
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterExercice", method = RequestMethod.POST)
	public ModelAndView createNewLecon(@Valid Exercice exercice, BindingResult bindingResult, MultipartFile image1,
			MultipartFile image2, MultipartFile image3, MultipartFile image4, MultipartFile audio1,
			MultipartFile audio2, MultipartFile audio3, MultipartFile audio4, MultipartFile audioEcouter,
			MultipartFile audio1ExerciceAssocier, MultipartFile audio2ExerciceAssocier,
			MultipartFile audio3ExerciceAssocier, MultipartFile audioPhraseOrdonner

	) {
		ModelAndView modelAndView = new ModelAndView();
		Exercice exerciceExists = exerciceService.findByTitreExercice(exercice.getTitreExercice());

		try {
			if (exercice != null && exercice.getExerciceQCM() != null) {
				ExerciceFiles exerciceFiles = new ExerciceFiles(image1, image2, image3, image4, audio1, audio2, audio3,
						audio4, audioEcouter, audio1ExerciceAssocier, audio2ExerciceAssocier, audio3ExerciceAssocier,
						audioPhraseOrdonner);
				setImagesAndAudiosToExercice(exercice, exerciceFiles);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (exerciceExists != null) {
			bindingResult.rejectValue("exercice", "error.exercice", "Ce exercice est d�j� existant");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("signup");
		} else {

			exerciceService.saveExercice(exercice);
			modelAndView.addObject("successMessage", "Le exercice est bien enregistr�");
			modelAndView.addObject("exercice", new Exercice());
			modelAndView.setViewName("AjouterExercice");

		}
		return modelAndView;
	}

	private void setImagesAndAudiosToExercice(Exercice exercice, ExerciceFiles exerciceFiles) throws Exception {
		exercice.getExerciceQCM().setImage1(fileHelper.encode(exerciceFiles.getFile1()));
		exercice.getExerciceQCM().setImage2(fileHelper.encode(exerciceFiles.getFile2()));
		exercice.getExerciceQCM().setImage3(fileHelper.encode(exerciceFiles.getFile3()));
		exercice.getExerciceQCM().setImage4(fileHelper.encode(exerciceFiles.getFile4()));
		exercice.getExerciceQCM().setAudio1(fileHelper.encode(exerciceFiles.getAudio1()));
		exercice.getExerciceQCM().setAudio2(fileHelper.encode(exerciceFiles.getAudio2()));
		exercice.getExerciceQCM().setAudio3(fileHelper.encode(exerciceFiles.getAudio3()));
		exercice.getExerciceQCM().setAudio4(fileHelper.encode(exerciceFiles.getAudio4()));
		exercice.getExerciceEcouter().setAudioEcouter(fileHelper.encode(exerciceFiles.getAudioEcouter()));
		exercice.getExerciceAssocier()
				.setAudio1ExerciceAssocier(fileHelper.encode(exerciceFiles.getAudio1ExerciceAssocier()));
		exercice.getExerciceAssocier()
				.setAudio1ExerciceAssocier(fileHelper.encode(exerciceFiles.getAudio2ExerciceAssocier()));
		exercice.getExerciceAssocier()
				.setAudio1ExerciceAssocier(fileHelper.encode(exerciceFiles.getAudio3ExerciceAssocier()));
		exercice.getExerciceOrdonner()
				.setAudioPhraseOrdonner((fileHelper.encode(exerciceFiles.getAudioPhraseOrdonner())));

	}

	@GetMapping("/deleteExercice/{id}")
	public String deleteLecon(@PathVariable("id") String id, Model model) {
		Exercice exercice = exerciceService.findById(id).orElseThrow();
		exerciceService.deleteExercice(exercice);
		return "redirect:/listExercice";
	}

	@GetMapping("/editExercice/{id}")
	public String showUpdateForm(@PathVariable("id") String id, Model model) {
		Exercice exercice = exerciceService.findById(id).orElseThrow();
		List<Lecon> LeconListe = leconRepository.findAll();
		model.addAttribute("lecons", LeconListe);
		model.addAttribute("exercice", exercice);
		return "updateExercice";
	}

	@PostMapping("/updateExercice/{id}")
	public String updateLecon(@PathVariable("id") String id, @Valid Exercice exercice, BindingResult result

	) throws IOException {
		if (result.hasErrors()) {
			exercice.setId(id);
			return "updateExercice";
		}

		exerciceService.saveExercice(exercice);
		return "redirect:/listExercice";
	}

}
