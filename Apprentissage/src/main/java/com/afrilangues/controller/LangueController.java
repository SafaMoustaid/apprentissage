package com.afrilangues.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afrilangues.domain.Langue;
import com.afrilangues.service.LangueService;
import com.afrilangues.utils.FileHelper;

@Controller
public class LangueController {

	@Autowired
	private LangueService langueService;
	@Autowired
	private FileHelper fileHelper;
	private static List<Langue> langues = new ArrayList<Langue>();

	@RequestMapping(value = "/listLangues", method = RequestMethod.GET)
	public ModelAndView AfficheLangue() {
		ModelAndView modelAndView = new ModelAndView();
		langues = langueService.getLangues();
		modelAndView.addObject("langues", langues);
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterLangues", method = RequestMethod.GET)
	public ModelAndView AjouterLangue() {
		ModelAndView modelAndView = new ModelAndView();
		Langue langue = new Langue();
		modelAndView.addObject("langue", langue);
		modelAndView.setViewName("AjouterLangues");
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterLangues", method = RequestMethod.POST)
	public ModelAndView createNewLangue(@Valid Langue langue, BindingResult bindingResult,
			@RequestParam("file") MultipartFile file) {
		ModelAndView modelAndView = new ModelAndView();
		Langue langueExists = langueService.findByNomLangue(langue.getNomLangue());

		try {
			langue.setImage(fileHelper.encode(file));

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (langueExists != null) {
			bindingResult.rejectValue("langue", "error.langue", "Cette langue est d�j� existant");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("signup");
		} else {
			langueService.saveLangue(langue);
			modelAndView.addObject("successMessage", "la langue est bien enregistr�");
			modelAndView.addObject("langue", new Langue());
			modelAndView.setViewName("AjouterLangues");
		}
		return modelAndView;
	}

	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") String id, Model model) {
		Langue langue = langueService.findById(id).orElseThrow();

		langueService.deleteLangue(langue);
		return "redirect:/listLangues";
	}

	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") String id, Model model) {
		Langue langue = langueService.findById(id).orElseThrow();

		model.addAttribute("langue", langue);
		return "updateLangue";
	}

	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable("id") String id, @RequestParam("nomLangue") String nomLangue,
			@RequestParam("file") MultipartFile file, @Valid Langue langue, BindingResult result, Model model)
			throws IOException {
		if (result.hasErrors()) {
			langue.setId(id);
			return "updateLangue";
		}
		langue.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
		langue.setNomLangue(nomLangue);
		langueService.saveLangue(langue);
		return "redirect:/listLangues";
	}
}
