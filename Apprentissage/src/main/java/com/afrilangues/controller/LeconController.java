package com.afrilangues.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afrilangues.domain.Lecon;
import com.afrilangues.domain.Theme;
import com.afrilangues.repository.ThemeRepository;
import com.afrilangues.service.LeconService;
import com.afrilangues.utils.FileHelper;

@Controller
public class LeconController {

	@Autowired
	private LeconService leconService;
	@Autowired
	private ThemeRepository themeRepository;
	@Autowired
	private FileHelper fileHelper;
	private static List<Lecon> lecons = new ArrayList<Lecon>();

	@RequestMapping(value = "/listLecon", method = RequestMethod.GET)
	public ModelAndView AfficheLecon() {
		ModelAndView modelAndView = new ModelAndView();
		lecons = leconService.getLecon();
		modelAndView.addObject("lecons", lecons);
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterLecon", method = RequestMethod.GET)
	public ModelAndView AjouterLecon() {
		List<Theme> themeListe = themeRepository.findAll();
		ModelAndView modelAndView = new ModelAndView();
		Lecon lecon = new Lecon();
		modelAndView.addObject("lecon", lecon);
		modelAndView.addObject("themes", themeListe);

		modelAndView.setViewName("AjouterLecon");
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterLecon", method = RequestMethod.POST)
	public ModelAndView createNewLecon(@Valid Lecon lecon, BindingResult bindingResult,
			@RequestParam("file") MultipartFile file) {
		ModelAndView modelAndView = new ModelAndView();

		Lecon leconExists = leconService.findByNomLecon(lecon.getNomLecon());
		try {
			lecon.setImage(fileHelper.encode(file));

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (leconExists != null) {
			bindingResult.rejectValue("le�on", "error.le�on", "Ce le�on est d�j� existant");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("signup");
		} else {
			leconService.saveLecon(lecon);
			modelAndView.addObject("successMessage", "Le le�on est bien enregistr�");
			modelAndView.addObject("lecon", new Lecon());
			modelAndView.setViewName("AjouterLecon");

		}
		return modelAndView;
	}

	@GetMapping("/deleteLecon/{id}")
	public String deleteLecon(@PathVariable("id") String id, Model model) {
		Lecon lecon = leconService.findById(id).orElseThrow();
		leconService.deleteLecon(lecon);
		return "redirect:/listLecon";
	}

	@GetMapping("/editLecon/{id}")
	public String showUpdateForm(@PathVariable("id") String id, Model model) {
		Lecon lecon = leconService.findById(id).orElseThrow();
		List<Theme> themesListe = themeRepository.findAll();
		model.addAttribute("themes", themesListe);

		model.addAttribute("lecon", lecon);
		return "updateLecon";
	}

	@PostMapping("/updateLecon/{id}")
	public String updateLecon(@PathVariable("id") String id, @RequestParam("nomLecon") String nomLecon,
			@RequestParam("file") MultipartFile file, @Valid Lecon lecon, BindingResult result, Model model)
			throws IOException {
		if (result.hasErrors()) {
			lecon.setId(id);

			return "updateTheme";
		}
		lecon.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
		lecon.setNomLecon(nomLecon);
		leconService.saveLecon(lecon);
		return "redirect:/listLecon";
	}

}
