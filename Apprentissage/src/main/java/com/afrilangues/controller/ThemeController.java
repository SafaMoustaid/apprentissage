package com.afrilangues.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afrilangues.domain.Langue;
import com.afrilangues.domain.Theme;
import com.afrilangues.repository.LangueRepository;
import com.afrilangues.service.ThemeService;
import com.afrilangues.utils.FileHelper;

@Controller

public class ThemeController {
	@Autowired
	private ThemeService themeService;
	@Autowired
	private LangueRepository langueRepository;
	@Autowired
	private FileHelper fileHelper;

	private static List<Theme> themes = new ArrayList<Theme>();

	@RequestMapping(value = "/listTheme", method = RequestMethod.GET)
	public ModelAndView AfficheTheme() {
		ModelAndView modelAndView = new ModelAndView();
		themes = themeService.getThemes();
		modelAndView.addObject("themes", themes);

		return modelAndView;
	}

	@RequestMapping(value = "/AjouterTheme", method = RequestMethod.GET)
	public ModelAndView AjouterTheme() {
		List<Langue> languesListe = langueRepository.findAll();
		ModelAndView modelAndView = new ModelAndView();
		Theme theme = new Theme();
		modelAndView.addObject("theme", theme);
		modelAndView.addObject("langues", languesListe);

		modelAndView.setViewName("AjouterTheme");
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterTheme", method = RequestMethod.POST)
	public ModelAndView createNewTheme(@Valid Theme theme, BindingResult bindingResult,
			@RequestParam("file") MultipartFile file) {
		ModelAndView modelAndView = new ModelAndView();
		Theme themeExists = themeService.findByNomTheme(theme.getNomTheme());
		try {
			theme.setImage(fileHelper.encode(file));

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (themeExists != null) {
			bindingResult.rejectValue("theme", "error.theme", "Ce th�me est d�j� existant");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("signup");
		} else {
			themeService.saveTheme(theme);
			modelAndView.addObject("successMessage", "Le theme est bien enregistr�");
			modelAndView.addObject("theme", new Theme());
			modelAndView.setViewName("AjouterTheme");
		}
		return modelAndView;
	}

	@GetMapping("/deleteTheme/{id}")
	public String deleteTheme(@PathVariable("id") String id, Model model) {
		Theme theme = themeService.findById(id).orElseThrow();
		themeService.deleteTheme(theme);
		return "redirect:/listTheme";
	}

	@GetMapping("/editTheme/{id}")
	public String showUpdateForm(@PathVariable("id") String id, Model model) {
		Theme theme = themeService.findById(id).orElseThrow();
		List<Langue> languesListe = langueRepository.findAll();
		model.addAttribute("langues", languesListe);
		model.addAttribute("theme", theme);
		return "updateTheme";
	}

	@PostMapping("/updateTheme/{id}")
	public String updateTheme(@PathVariable("id") String id, @RequestParam("nomTheme") String nomTheme,
			@RequestParam("file") MultipartFile file, @Valid Theme theme, BindingResult result, Model model)
			throws IOException {
		if (result.hasErrors()) {
			theme.setId(id);
			return "updateTheme";
		}
		theme.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
		theme.setNomTheme(nomTheme);
		themeService.saveTheme(theme);
		return "redirect:/listTheme";
	}

}

//	
