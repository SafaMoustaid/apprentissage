package com.afrilangues.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.afrilangues.domain.Langue;
import com.afrilangues.domain.Role;
import com.afrilangues.domain.User;
import com.afrilangues.repository.LangueRepository;
import com.afrilangues.repository.RoleRepository;
import com.afrilangues.service.CustomUserDetailsService;

@Controller
public class UtilisateurController {

	@Autowired
	private CustomUserDetailsService userService;
	@Autowired
	private LangueRepository langueRepository;

	@Autowired
	private RoleRepository roleRepository;

	private static List<User> users = new ArrayList<User>();

	@RequestMapping(value = "/listUtilisateur", method = RequestMethod.GET)
	public ModelAndView AfficheUtilisateur() {
		ModelAndView modelAndView = new ModelAndView();
		users = userService.getUsers();
		modelAndView.addObject("users", users);
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterUtilisateur", method = RequestMethod.GET)
	public ModelAndView AjouterLangue() {
		List<Langue> languesListe = langueRepository.findAll();
		List<Role> roleListe = roleRepository.findAll();
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.addObject("langues", languesListe);
		modelAndView.addObject("roles", roleListe);
		modelAndView.setViewName("AjouterUtilisateur");
		return modelAndView;
	}

	@RequestMapping(value = "/AjouterUtilisateur", method = RequestMethod.POST)
	public ModelAndView createNewUtilisateur(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());

		if (userExists != null) {
			bindingResult.rejectValue("user", "error.user", "Ce utilisateur est d�j� existant");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("AjouterUtilisateur");
		} else {
			userService.saveUtilisateur(user);
			modelAndView.addObject("successMessage", "L'utilisateur est bien enregistr�");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("AjouterUtilisateur");

		}
		return modelAndView;
	}

	@GetMapping("/deleteUser/{id}")
	public String deleteUtilisateur(@PathVariable("id") String id, Model model) {
		User user = userService.findById(id).orElseThrow();
		userService.deleteUser(user);
		return "redirect:/listUtilisateur";
	}

	@GetMapping("/editUser/{id}")
	public String showUpdateForm(@PathVariable("id") String id, Model model) {
		User user = userService.findById(id).orElseThrow();
		List<Langue> languesListe = langueRepository.findAll();
		List<Role> roleListe = roleRepository.findAll();
		model.addAttribute("langues", languesListe);
		model.addAttribute("roles", roleListe);
		model.addAttribute("user", user);
		return "updateUser";
	}

	@PostMapping("/updateUser/{id}")
	public String updateLecon(@PathVariable("id") String id, @Valid User user, BindingResult result

	) throws IOException {
		if (result.hasErrors()) {
			user.setId(id);
			return "updateUser";
		}

		userService.saveUtilisateur(user);
		return "redirect:/listUser";
	}
}
