
package com.afrilangues.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author safa
 */
@Configuration
public class PageConfig implements WebMvcConfigurer {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/dashboard").setViewName("dashboard");
        registry.addViewController("/dashboard2").setViewName("dashboard2");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/AjouterLangues").setViewName("AjouterLangues");
        registry.addViewController("/gererLangue").setViewName("gererLangue");
        registry.addViewController("/theme").setViewName("theme");
        registry.addViewController("/listLangues").setViewName("listLangues");
        registry.addViewController("/menu").setViewName("menu");
        registry.addViewController("/updateLangue").setViewName("updateLangue");
        registry.addViewController("/AjouterTheme").setViewName("AjouterTheme");

    }
    @Override
    public void addResourceHandlers (ResourceHandlerRegistry registry) {
        registry.addResourceHandler ("/ image/**")
                .addResourceLocations ("classpath:/static/image/");
    }
}
